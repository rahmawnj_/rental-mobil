<?php
class User_model extends CI_Model
{
    public function get_users()
    {
        return $this->db->get('users')->result_array();
    }

    public function get_user($fields, $data)
    {
        return $this->db->get_where('users', [$fields => $data])->result_array();
    }
}
