<?php
class Log_lokasi_model extends CI_Model
{
    public function get_log_lokasis()
    {
        return $this->db->get('log_lokasi')->result_array();
    }

    public function get_log_lokasi($fields, $data)
    {
        return $this->db->get_where('log_lokasi', [$fields => $data])->result_array();
    }
}
