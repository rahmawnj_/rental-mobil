<?php
class Mobil_model extends CI_Model
{
    public function get_mobils()
    {
        return $this->db->get('mobil')->result_array();
    }

    public function get_mobil($fields, $data)
    {
        return $this->db->get_where('mobil', [$fields => $data])->result_array();
    }
}
