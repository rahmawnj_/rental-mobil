<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard'] = 'dashboard/index';
$route['dashboard/akun'] = 'users/akun';
$route['dashboard/setting_area'] = 'area/setting_area';

$route['dashboard/mobil'] = 'mobil/mobil';
$route['dashboard/tambah_mobil'] = 'mobil/tambah_mobil';
$route['dashboard/ubah_mobil/(:any)'] = 'mobil/ubah_mobil/$1';
$route['dashboard/detail_mobil/(:any)'] = 'mobil/detail_mobil/$1';
$route['dashboard/delete_mobil/(:any)'] = 'mobil/delete_mobil/$1';
$route['dashboard/status_mesin/(:any)'] = 'mobil/status_mesin/$1';
$route['dashboard/mode_mesin/(:any)'] = 'mobil/mode_mesin/$1';

$route['dashboard/log_lokasi'] = 'log_lokasi/log_lokasi';
$route['dashboard/log_lokasi_detail/(:any)'] = 'log_lokasi/log_lokasi_detail/$1';
$route['dashboard/log_lokasi_map/(:any)'] = 'log_lokasi/log_lokasi_map/$1';

$route['dashboard/setting'] = 'setting/setting';
$route['dashboard/setting_gmaps'] = 'setting/setting_gmaps';
$route['dashboard/tes_telegram'] = 'setting/tes_telegram';
$route['dashboard/setting_telegram'] = 'setting/setting_telegram';
