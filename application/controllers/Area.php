<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    if (!$this->session->userdata('username')) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Terlebih Dahulu!</div>');
      redirect('auth/index');
    }
  }

  public function setting_area()
  {
    $data = [
      'title' => 'Pengaturan Area',
      'gmaps' => $this->db->get_where('setting', ['label' => 'gmaps_api_key'])->row_array()
    ];
    return $this->load->view('dashboard/setting_area', $data);
  }

}