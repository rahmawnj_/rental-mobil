<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
  public function __construct()
  {
    parent::__construct();
  }

  public function setting_area_post()
  {
    $this->db->empty_table('area');
    for ($i = 0; $i < count($this->input->post('coordinates')); $i++) {
      $this->db->insert('area', [
        'latitude' => explode(',', $this->input->post('coordinates')[$i])[0],
        'longitude' => explode(',', $this->input->post('coordinates')[$i])[1],
      ]);
    }
  }

  public function pin_get()
  {
    echo json_encode($this->db->get('mobil')->result());
  }

  public function area_get()
  {
    echo json_encode($this->db->get("area")->result());
  }
  public function track_filter_post()
  {
    echo json_encode($this->db->get('log_lokasi')->result());
  }

  public function log_lokasi_map_filter_post()
  {
    $lk = $this->db->where('id_device', $this->input->post('id_device'));
    $lk = $this->db->where('waktu >=', date('Y-m-d', strtotime($this->input->get('From'))));
    $lk = $this->db->where('waktu <=', date('Y-m-d', strtotime($this->input->get('To'))));
    $lk = $this->db->get('log_lokasi')->result_array();
    echo json_encode($lk);
  }
  public function track_get()
  {
    echo json_encode($this->db->get('log_lokasi')->result());
  }

  public function index_get()
  {
    if (is_null($this->input->get('id_device', true))) {
      $this->response(['msg' => 'id_device perlu dikirim', 'status' => RestController::HTTP_NOT_FOUND]);
    }
    if (is_null($this->input->get('latitude', true))) {
      $this->response(['msg' => 'latitude perlu dikirim', 'status' => RestController::HTTP_NOT_FOUND]);
    }
    if (is_null($this->input->get('longitude', true))) {
      $this->response(['msg' => 'longitude perlu dikirim', 'status' => RestController::HTTP_NOT_FOUND]);
    }
    if (is_null($this->input->get('status_relay', true))) {
      $this->response(['msg' => 'status_relay perlu dikirim', 'status' => RestController::HTTP_NOT_FOUND]);
    }
    $mobil = $this->db->get_where('mobil', ['id_device' => $this->input->get('id_device', true)])->row_array();
    if (!$mobil) {
      $this->response(['msg' => 'id device ' . $this->input->get('id_device', true) . ' tidak ditemukan', 'status' => RestController::HTTP_OK]);
    } else {
      $area = $this->db->get('area')->result_array();
      $koordinats = [];
      for ($i = 0; $i < count($area); $i++) {
        $koordinats[] = implode(',', [$area[$i]['latitude'], $area[$i]['longitude']]);
      }
      $vertices_x = [];
      $vertices_y = [];
      for ($iv = 0; $iv < count($koordinats); $iv++) {
        $vertices_y[] = explode(',', $koordinats[$iv])[0];
        $vertices_x[] = explode(',', $koordinats[$iv])[1];
      }
      $points_polygon = count($vertices_x);
      if ($this->input->get('longitude', true) == 0 && $this->input->get('latitude', true) == 0) {
        $last_log = $this->db->get_where('log_lokasi', ['id_device' => $this->input->get('id_device', true)])->last_row();
        $latitude_y = $last_log->latitude;
        $longitude_x = $last_log->longitude;
      } else {
        $longitude_x = $this->input->get('longitude', true);
        $latitude_y = $this->input->get('latitude', true);
      }
      $checkStatus = $this->point_position($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y);
      if ($checkStatus) {
        $status = 'Dalam Area';
      } else {
        $status = 'Luar Area';
      }
      $this->db->insert('log_lokasi', [
        'id_device' => $this->input->get('id_device', true),
        'latitude' => $latitude_y,
        'longitude' => $longitude_x,
        'status_relay' => $this->input->get('status_relay', true),
        'status'  => $status,
        'waktu' => date('Y-m-d H:i:s')
      ]);
      if (!$this->point_position($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)) {
      
        $token = $this->db->get_where('setting', ['label' => 'telegram_token'])->row_array()['value'];
        $id_chat = $this->db->get_where('setting', ['label' => 'telegram_id_chat'])->row_array()['value'];
        $data = [
          'text' => "Mobil plat nomor " . $mobil['plat_nomor'] . " telah keluar dari zona",
          'chat_id' => $id_chat
        ];
        file_get_contents("https://api.telegram.org/bot$token/sendMessage?" . http_build_query($data), false);
        if ($mobil['mode'] == 'auto') {
          $status_mesin = 'off';
          $sm = '0';
          $this->db->update('mobil', ['status_mesin' => $sm, 'latitude' => $latitude_y, 'longitude' => $longitude_x, 'status_relay' => $this->input->get('status_relay', true), 'last_update' => date('Y-m-d H:i:s')], ['id_mobil' => $mobil['id_mobil']]);
          $this->response(['status' => false, 'status_device' => 'tidak dalam area', 'status_mesin' => $status_mesin, 'mode' =>  $mobil['mode']], RestController::HTTP_OK);
        } else {

          if ($mobil['status_mesin'] == '0') {
            $status_mesin = "off";
            $sm = '0';
          } else {
            $status_mesin = "on";
            $sm = '1';
          }
          $this->db->update('mobil', ['status_mesin' => $sm, 'latitude' => $latitude_y, 'longitude' => $longitude_x, 'status_relay' => $this->input->get('status_relay', true), 'last_update' => date('Y-m-d H:i:s')], ['id_mobil' => $mobil['id_mobil']]);
          $this->response(['status' => false, 'status_device' => 'tidak dalam area', 'status_mesin' => $status_mesin, 'mode' =>  $mobil['mode']], RestController::HTTP_OK);
        }
      } else {
        // Dalam Area
        if ($mobil['mode'] == 'auto') {
          $this->db->update('mobil', ['status_mesin' => '1', 'latitude' => $latitude_y, 'longitude' => $longitude_x, 'status_relay' => $this->input->get('status_relay', true), 'last_update' => date('Y-m-d H:i:s')], ['id_mobil' => $mobil['id_mobil']]);
          $this->response(['status' => false, 'status_device' => 'dalam area', 'status_mesin' => 'on', 'mode' =>  $mobil['mode']], RestController::HTTP_OK);
        } else {
          if ($mobil['status_mesin'] == '0') {
            $status_mesin = "off";
            $sm = '0';
          } else {
            $status_mesin = "on";
            $sm = '1';
          }
          $this->db->update('mobil', ['status_mesin' => $sm, 'latitude' => $latitude_y, 'longitude' => $longitude_x, 'status_relay' => $this->input->get('status_relay', true), 'last_update' => date('Y-m-d H:i:s')], ['id_mobil' => $mobil['id_mobil']]);
          $this->response(['status' => false, 'status_device' => 'dalam area', 'status_mesin' => $status_mesin, 'mode' =>  $mobil['mode']], RestController::HTTP_OK);
        }
      }
    }
  }
  function point_position($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
  {
    $i = $j = $c = 0;
    for ($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i++) {
      if ((($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
        ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i])))
        $c = !$c;
    }
    return $c;
  }

  function log_lokasi_post()
  {
    $result = '';
    $lk = $this->db->where('id_device', $this->input->post('id_device'));
    $lk = $this->db->where('waktu >=', date('Y-m-d', strtotime($this->input->get('From'))));
    $lk = $this->db->where('waktu <=', date('Y-m-d', strtotime($this->input->get('To'))));
    $lk = $this->db->get('log_lokasi')->result_array();
    $result .= '
    <table class="table table-hover my-0">
    <tr>
    <th>No. </th>
    <th>Latitude</th>
    <th>Longitude</th>
    <th>Status</th>
    <th>Waktu</th>
    </tr>';
    if (count($lk)) {
      $no = 0;
      foreach ($lk as $row) {
        $result .= '
            <tr>
            <td>' . ++$no . '</td>
            <td>' . $row["latitude"] . '</td>
            <td>' . $row["longitude"] . '</td>
            <td>' . $row["status"] . '</td>
            <td>' . $row["waktu"] . '</td>
            </tr>';
      }
    } else {
      $result .= '
        <tr>
        <td colspan="5">No Purchased Item Found</td>
        </tr>';
    }
    $result .= '</table>';
    echo $result;
  }
}
