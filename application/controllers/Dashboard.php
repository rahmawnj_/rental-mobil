<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    if (!$this->session->userdata('username')) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Terlebih Dahulu!</div>');
      redirect('auth/index');
    }
  }

  public function index()
  {
    $data = [
      'area' => json_encode($this->db->get("area")->result()),
      'title' => 'Dashboard',
      'gmaps' => $this->db->get_where('setting', ['label' => 'gmaps_api_key'])->row_array()
    ];
    return $this->load->view('dashboard/index', $data);
  }
  
}
