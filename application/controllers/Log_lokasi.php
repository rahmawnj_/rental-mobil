<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Log_lokasi extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('mobil_model');
    if (!$this->session->userdata('username')) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Terlebih Dahulu!</div>');
      redirect('auth/index');
    }
  }

  public function log_lokasi()
  {
    $data = [
      'title' => 'Log Lokasi',
      'mobil' => $this->db->get('mobil')->result_array()
    ];
    $this->load->view('dashboard/log_lokasi', $data);
  }

  public function log_lokasi_detail($id_device)
  {

    if ($this->input->GET('To', TRUE) && $this->input->GET('From', TRUE)) {
      $lk = $this->db->where('id_device', $id_device);
      $lk = $this->db->where('waktu >=', date('Y-m-d', strtotime($this->input->get('From'))));
      $lk = $this->db->where('waktu <=', date('Y-m-d', strtotime($this->input->get('To'))));
      $lk = $this->db->get('log_lokasi')->result_array();
    } else {
      $lk = $this->db->get_where('log_lokasi', ['id_device' => $id_device])->result_array();
    }

    $data = [
      'title' => 'Log Lokasi',
      'mobil' => $this->db->get_where('mobil', ['id_device' => $id_device])->row_array(),
      'log_lokasi' => $lk
    ];
    $this->load->view('dashboard/log_lokasi_detail', $data);
  }
  

  public function log_lokasi_map($id_device)
  {
    if ($this->input->GET('To', TRUE) && $this->input->GET('From', TRUE)) {
      $lk = $this->db->where('id_device', $id_device);
      $lk = $this->db->where('waktu >=', date('Y-m-d', strtotime($this->input->get('From'))));
      $lk = $this->db->where('waktu <=', date('Y-m-d', strtotime($this->input->get('To'))));
      $lk = $this->db->get('log_lokasi')->result_array();
    } else {
      $lk = $this->db->get_where('log_lokasi', ['id_device' => $id_device])->result_array();
    }
    $data = [
      'title' => 'Log Lokasi',
      'mobil' => $this->db->get_where('mobil', ['id_device' => $id_device])->row_array(),
      'log_lokasi' => json_encode($lk),
      'gmaps' => $this->db->get_where('setting', ['label' => 'gmaps_api_key'])->row_array()
    ];
 
    $this->load->view('dashboard/log_lokasi_map', $data);
  }


}