<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function log_lokasi_detail($id_device)
	{
	
		$spreadsheet = new Spreadsheet();

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('B1', 'Latitude')
			->setCellValue('C1', 'Longitude')
			->setCellValue('D1', 'Waktu');

		$i = 2;
		if ($this->input->GET('To', TRUE) && $this->input->GET('From', TRUE)) {
			$log_lokasi = $this->db->where('id_device', $id_device);
			$log_lokasi = $this->db->where('waktu >=', date('Y-m-d', strtotime($this->input->get('From'))));
			$log_lokasi = $this->db->where('waktu <=', date('Y-m-d', strtotime($this->input->get('To'))));
			$log_lokasi = $this->db->get('log_lokasi')->result();
		} else {
			$log_lokasi = $this->db->get_where('log_lokasi', ['id_device' => $id_device])->result();
		}

		foreach ($log_lokasi as $row) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('B' . $i, $row->latitude)
				->setCellValue('C' . $i, $row->longitude)
				->setCellValue('D' . $i, $row->waktu);
			$i++;
		}

		$spreadsheet->getActiveSheet()->setTitle('Report id device' . $id_device);
		$spreadsheet->setActiveSheetIndex(0);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		if ($this->input->GET('To', TRUE) && $this->input->GET('From', TRUE)) {
			header('Content-Disposition: attachment;filename="Report="' . $id_device . '-' . date('Y-m-d', strtotime($this->input->get('To'))) . ' - ' . date('Y-m-d', strtotime($this->input->get('From'))) . '.xlsx"');
		} else {
			header('Content-Disposition: attachment;filename="Report="' . $id_device . '-' . date('Y-m-d') . '.xlsx"');
		}
		header('Cache-Control: max-age=0');

		header('Cache-Control: max-age=1');

		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}
