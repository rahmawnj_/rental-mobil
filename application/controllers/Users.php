<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('user_model');

    if (!$this->session->userdata('username')) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Terlebih Dahulu!</div>');
      redirect('auth/index');
    }
  }

  public function akun()
  {
    $data = [
      'title' => 'Akun',
      'user' => $this->user_model->get_users()[0],
    ];

    $this->load->view('dashboard/akun', $data);
  }
}