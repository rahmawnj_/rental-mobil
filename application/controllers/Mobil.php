<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mobil extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('mobil_model');
    if (!$this->session->userdata('username')) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Terlebih Dahulu!</div>');
      redirect('auth/index');
    }
  }

  public function tambah_mobil()
  {
    $data = [
      'title' => 'Mobil',
      'method' => 'POST',
      'action' => base_url('mobil/store_mobil')
    ];
    $this->load->view('dashboard/tambah_mobil', $data);
  }
  public function ubah_mobil($id)
  {
    $data = [
      'title' => 'Mobil',
      'method' => 'POST',
      'action' => base_url('mobil/store_mobil'),
      'mobil' => $this->mobil_model->get_mobil('id_mobil', $id)[0]
    ];
   
    $this->load->view('dashboard/ubah_mobil', $data);
  }
  public function detail_mobil($id)
  {
    $data = [
      'title' => 'Mobil',
      'mobil' => $this->mobil_model->get_mobil('id_mobil', $id)[0]
    ];
    $this->load->view('dashboard/detail_mobil', $data);
  }

  public function store_mobil()
  {
    $this->form_validation->set_rules('id_device', 'ID Device', 'required|trim');
    $this->form_validation->set_rules('plat_nomor', 'Plat Nomor', 'required|trim');
    $this->form_validation->set_rules('latitude', 'Latitude', 'required|trim');
    $this->form_validation->set_rules('longitude', 'Longitude', 'required|trim');

    if ($this->form_validation->run() == false) {
      $data = [
        'title' => 'Rental Mobil | Form Mobil',
        'method' => 'POST',
        'action' => base_url('dashboard/store_mobil')
      ];
      if (is_null($this->input->post('id_mobil'))) {
        $this->load->view('dashboard/tambah_mobil', $data);
      } else {
        $data['mobil'] = $this->mobil_model->get_mobil('id_mobil', $this->input->post('id_mobil'))[0];
        $this->load->view('dashboard/ubah_mobil', $data);
      }
    } else {
      if (is_null($this->input->post('id_mobil'))) {
        $this->db->insert('mobil', [
          'mode' => 'auto',
          'id_device' => $this->input->post('id_device'),
          'plat_nomor' => $this->input->post('plat_nomor'),
          'latitude' => $this->input->post('latitude'),
          'longitude' => $this->input->post('longitude'),
        ]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
              Data Berhasil Ditambahkan!
            </div>');
        redirect('dashboard/mobil');
      } else {
        $this->db->update('mobil', [
          'id_device' => $this->input->post('id_device'),
          'plat_nomor' => $this->input->post('plat_nomor'),
          'latitude' => $this->input->post('latitude'),
          'longitude' => $this->input->post('longitude'),
        ], ['id_mobil' => $this->input->post('id_mobil')]);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
              Data Berhasil Diubah!
            </div>');
        redirect('dashboard/mobil');
      }
    }
  }

  public function delete_mobil($id)
  {
    $this->db->where('id_mobil', $id);
    $this->db->delete('mobil');
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data Berhasil Dihapus!</div>');
    redirect('dashboard/mobil');
  }

  public function mobil()
  {
    $data = [
      'title' => 'Mobil',
      'mobil' => $this->mobil_model->get_mobils()
    ];
    $this->load->view('dashboard/mobil', $data);
  }

  public function status_mesin($id)
  {
    $mobil = $this->mobil_model->get_mobil('id_mobil', $id)[0];
  
    $this->db->update('mobil', [
      'status_mesin' => $this->input->post('status_mesin'),
      'last_update' => $mobil['last_update']
    ], ['id_mobil' => $id]);
    if ($this->input->post('status_mesin') == '1') {
      $status = 'Hidupkan';
    } else {
      $status = 'Matikan';
    }
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
          Mesin Berhasil ' . $status . '!
        </div>');
    redirect('dashboard/mobil');
  }

  public function mode_mesin($id)
  {
    $mobil = $this->mobil_model->get_mobil('id_mobil', $id)[0];

    $this->db->update('mobil', [
      'mode' => $this->input->post('mode_mesin'),
      'last_update' => $mobil['last_update']
    ], ['id_mobil' => $id]);

    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
          Mode mesin berhasil diubah menjadi ' . $this->input->post('mode_mesin') . '!
        </div>');
    redirect('dashboard/mobil');
  }
}