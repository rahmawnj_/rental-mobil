<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		if ($this->session->userdata('username')) {
			redirect('dashboard');
		}
		$data['title'] = 'Login';
		$this->load->view('templates/auth_header', $data);
		$this->load->view('auth/login');
		$this->load->view('templates/auth_footer');
	}

	public function change_profile()
	{
		$this->form_validation->set_rules('name', 'Nama', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		if ($this->form_validation->run() == false) {
			redirect('dashboard/akun');
		} else {
			if ($this->input->post('password') !== $this->input->post('password_verify')) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
					Password tidak sama!
				</div>');
				redirect('dashboard/akun');
			} else {
				if ($this->input->post('password')) {
					$this->db->update('users', [
						'name' => $this->input->post('name'),
						'username' => $this->input->post('username'),
						'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					], ['id_user' => $this->input->post('id_user')]);
					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
						Akun berhasil diubah!
					</div>');
				} else {
					$user = $this->db->get_where('users')->result_array()[0];
					$this->db->update('users', [
						'name' => $this->input->post('name'),
						'username' => $this->input->post('username'),
						'password' => $user['password']
					], ['id_user' => $this->input->post('id_user')]);
					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
						Akun berhasil diubah!
					</div>');
				}
				redirect('dashboard/akun');
			}
		}
	}

	public function login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user = $this->db->get_where('users', ['username' => $username])->row_array();

			if ($user) {
				if (password_verify($password, $user['password'])) {
					$data = [
						'username' => $user['username'],
					];
					$this->session->set_userdata($data);
					redirect('dashboard');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
						Password Salah!
					</div>');
					redirect('auth/login');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				Tidak Terdaftar
			  </div>');
				redirect('auth/login');
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		redirect('auth/login');
	}
}
