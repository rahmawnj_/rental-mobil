<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    if (!$this->session->userdata('username')) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Login Terlebih Dahulu!</div>');
      redirect('auth/index');
    }
  }

  public function setting_telegram()
  {
    $this->form_validation->set_rules('telegram_token', 'Telegram Token', 'required|trim');
    $this->form_validation->set_rules('telegram_id_chat', 'Telagram Id Chat', 'required|trim');
    if ($this->form_validation->run() == false) {
      $data = [
        'title' => 'Setting',
        'gmaps' => $this->db->get_where('setting', ['label' => 'gmaps_api_key'])->row_array(),
        'telegram_token' => $this->db->get_where('setting', ['label' => 'telegram_token'])->row_array(),
        'telegram_id_chat' => $this->db->get_where('setting', ['label' => 'telegram_id_chat'])->row_array(),
      ];
      $this->load->view('dashboard/setting', $data);
    } else {
      $this->db->update('setting', [
        'value' => $this->input->post('telegram_token'),
      ], ['id_setting' => $this->input->post('id_telegram_token')]);
      $this->db->update('setting', [
        'value' => $this->input->post('telegram_id_chat'),
      ], ['id_setting' => $this->input->post('id_telegram_id_chat')]);

      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-success" role="alert">
          Data Berhasil Diubah!
        </div>'
      );
      redirect('dashboard/setting');
    }
  }

  public function tes_telegram()
  {
    try {
      $token = $this->db->get_where('setting', ['label' => 'telegram_token'])->row_array()['value'];
      $id_chat = $this->db->get_where('setting', ['label' => 'telegram_id_chat'])->row_array()['value'];

      $data = [
        'text' => "Tes",
        'chat_id' => $id_chat
      ];
      $response = file_get_contents("https://api.telegram.org/bot$token/sendMessage?" . http_build_query($data), false);
      echo $response;
    } catch (\Throwable $th) {
      echo 'Pesan Gagal Terkirim';
    }
  }

  public function setting()
  {
    $data = [
      'title' => 'Pengaturan',
      'gmaps' => $this->db->get_where('setting', ['label' => 'gmaps_api_key'])->row_array(),
      'telegram_token' => $this->db->get_where('setting', ['label' => 'telegram_token'])->row_array(),
      'telegram_id_chat' => $this->db->get_where('setting', ['label' => 'telegram_id_chat'])->row_array(),
    ];
    $this->load->view('dashboard/setting', $data);
  }

  public function setting_gmaps()
  {
    $this->form_validation->set_rules('gmaps_api_key', 'GMAPS API KEY', 'required|trim');
    if ($this->form_validation->run() == false) {
      $data = [
        'title' => 'Setting',
        'gmaps' => $this->db->get_where('setting', ['label' => 'gmaps_api_key'])->row_array(),
        'telegram_token' => $this->db->get_where('setting', ['label' => 'telegram_token'])->row_array(),
        'telegram_id_chat' => $this->db->get_where('setting', ['label' => 'telegram_id_chat'])->row_array(),
      ];
      $this->load->view('dashboard/setting', $data);
    } else {
      $this->db->update('setting', [
        'value' => $this->input->post('gmaps_api_key'),
      ], ['id_setting' => $this->input->post('id_setting')]);
      $this->session->set_flashdata(
        'message',
        '<div class="alert alert-success" role="alert">
          Data Berhasil Diubah!
        </div>'
      );
      redirect('dashboard/setting');
    }
  }
}