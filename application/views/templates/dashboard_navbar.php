<nav class="navbar navbar-expand navbar-light navbar-bg" id="navbarTop"><a class="sidebar-toggle js-sidebar-toggle">
        <i class="hamburger align-self-center"></i>
    </a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav navbar-align">
            <li>
                <h4>
                    <b class="align-self-center">
                        <?= $this->db->get_where('users')->result_array()[0]['name'] ?>
                    </b>
                </h4>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-icon pe-md-0 dropdown-toggle" href="#" data-bs-toggle="dropdown">
                    <span class="fa fa-gear"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= base_url('dashboard/setting') ?>">
                        <span class="fa fa-gear"></span>
                        Pengaturan
                    </a>
                    <a class="dropdown-item" href="<?= base_url('dashboard/akun') ?>">
                        <span class="fa fa-user"></span>
                        Akun
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">
                    <span class="fa fa-sign-out"></span>
                        Log out
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>