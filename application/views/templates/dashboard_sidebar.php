<nav class="sidebar js-sidebar" id="sidebar">
    <div class="sidebar-content js-simplebar" data-simplebar="init">
        <div class="simplebar-wrapper" style="margin: 0px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;" tabindex="0" role="region" aria-label="scrollable content">
                        <div class="simplebar-content" style="padding: 0px;">
                            <a class="sidebar-brand" href="javascript:;">
                                <span>Rental Mobil</span>
                            </a>

                            <ul class="sidebar-nav">
                                <li class="sidebar-header">
                                    Menu
                                </li>
                                <li class="sidebar-item <?php if ($title == 'Dashboard') : ?> active <?php endif ?> ">
                                    <a class="sidebar-link" href="<?= base_url('dashboard') ?>">
                                        <span class="fa fa-dashboard"></span>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <li class="sidebar-item <?php if ($title == 'Mobil') : ?> active <?php endif ?>">
                                    <a class="sidebar-link" href="<?= base_url('dashboard/mobil') ?>">
                                        <span class="fa fa-car"></span>
                                        <span>Mobil</span>
                                    </a>
                                </li>
                                <li class="sidebar-item <?php if ($title == 'Pengaturan Area') : ?> active <?php endif ?>">
                                    <a class="sidebar-link" href="<?= base_url('dashboard/setting_area') ?>">
                                        <span class="fa fa-area-chart"></span>
                                        <span>Pengaturan Area</span>
                                    </a>
                                </li>
                                <li class="sidebar-item <?php if ($title == 'Pengaturan') : ?> active <?php endif ?>">
                                    <a class="sidebar-link" href="<?= base_url('dashboard/setting') ?>">
                                        <span class="fa fa-gear"></span>
                                        <span>Pengaturan</span>
                                    </a>
                                </li>
                                <!-- <li class="sidebar-header">
                                    Laporan
                                </li>
                                <li class="sidebar-item <?php if ($title == 'Log Lokasi') : ?> active <?php endif ?>">
                                    <a class="sidebar-link" href="<?= base_url('dashboard/log_lokasi') ?>">
                                        <span class="fa fa-history"></span>
                                        <span>Log Lokasi</span>
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="simplebar-placeholder" style="width: auto; height: 967px;"></div>
        </div>

    </div>
</nav>