<div class="container">
  <div class="row content">
    <div class="col-md-6 mb-3">
      <img src="<?= base_url('assets/img/undraw_Trip_re_f724.png') ?>" class="img-fluid" alt="">
    </div>
    <div class="col-md-6">
      <h3 class="signin-text mb-3"> Login </h3>
      <form action="<?= base_url('auth/login') ?>" method="post">
        <?= $this->session->flashdata('message'); ?>
        <div class="form-group mb-3">
          <label for="username">Username</label>
          <input type="username" name="username" value="<?= set_value('username') ?>" class="form-control">
          <span class="text-danger">
            <?= form_error('username') ?>
          </span>
        </div>
        <div class="form-group mb-3">
          <label for="password">Password</label>
          <input type="password" name="password" value="<?= set_value('password') ?>" class="form-control">
          <span class="text-danger">
            <?= form_error('password') ?>
          </span>
        </div>
        <button type="submit" class="btn btn-class">Login</button>
      </form>
    </div>
  </div>
</div>