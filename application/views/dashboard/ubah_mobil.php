<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="card">
                    <div class="card-header">
                        Form Mobil
                    </div>
                    <div class="card-body">
                        <form action="<?= $action ?>" method="<?= $method ?>">
                            <?= $this->session->flashdata('message'); ?>
                            <input type="hidden" name="id_mobil" value="<?= $mobil['id_mobil'] ?>" class="form-control" id="id_mobil">

                            <div class="mb-3">
                                <label for="id_device" class="form-label">ID Device</label>
                                <input type="id_device" name="id_device" value="<?= $mobil['id_device'] ?>" class="form-control" id="id_device">
                                <span class="text-danger">
                                    <?= form_error('id_device') ?>
                                </span>
                            </div>
                            <div class="mb-3">
                                <label for="plat_nomor" class="form-label">Plat Nomor</label>
                                <input type="plat_nomor" name="plat_nomor" value="<?= $mobil['plat_nomor'] ?>" class="form-control" id="plat_nomor">
                                <span class="text-danger">
                                    <?= form_error('plat_nomor') ?>
                                </span>
                            </div>
                            <div class="mb-3">
                                <label for="latitude" class="form-label">Latitude</label>
                                <input type="latitude" name="latitude" class="form-control" value="<?= $mobil['latitude'] ?>" id="latitude">
                                <span class="text-danger">
                                    <?= form_error('latitude') ?>
                                </span>
                            </div>
                            <div class="mb-3">
                                <label for="longitude" class="form-label">Longitude</label>
                                <input type="longitude" name="longitude" class="form-control" value="<?= $mobil['longitude'] ?>" id="longitude">
                                <span class="text-danger">
                                    <?= form_error('longitude') ?>
                                </span>
                            </div>
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary mb-3">Kirim</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php $this->load->view('templates/dashboard_footer') ?>