<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="card">
                    <div class="card-header">
                        <b>
                            Plat Nomor <?= $mobil['plat_nomor'] ?>
                        </b>
                    </div>
                    <div class="card-body">
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Id Device</div>
                            <div class="col-8"><?= $mobil['id_device'] ?></div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Plat Nomor</div>
                            <div class="col-8"><?= $mobil['plat_nomor'] ?></div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Status Mesin</div>
                            <div class="col-8">
                                <span <?php if ($mobil['status_mesin']) : ?> class="badge bg-success" <?php else : ?> class="badge bg-danger" <?php endif ?>>
                                    <?php if ($mobil['status_mesin']) : ?> Hidup <?php else : ?> Mati <?php endif ?>
                                </span>
                            </div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Mode</div>
                            <div class="col-8">
                                <span <?php if ($mobil['mode'] == 'auto') : ?> class="badge bg-info" <?php else : ?> class="badge bg-warning" <?php endif ?>>
                                    <?= ($mobil['mode']) ?>
                                </span>
                            </div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Status Relay</div>
                            <div class="col-8"><?= $mobil['status_relay'] ?></div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Koordinat</div>
                            <div class="col-8"><?= $mobil['latitude'] ?>, <?= $mobil['longitude'] ?></div>
                        </div>
                        <div class="row mt-2 mb-2">
                            <div class="col-4">Terakhir Diperbarui</div>
                            <div class="col-8"><?= $mobil['last_update'] ?></div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <b>
                            Plat Nomor <?= $mobil['plat_nomor'] ?>
                        </b>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php $this->load->view('templates/dashboard_footer') ?>