<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="col-lg-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <form action="" method="get">
                                    <div class="input-group">
                                        <div class="col">
                                            <input type="date" value="<?= $this->input->get('From', TRUE) ?>" name="From" id="" class="form-control">
                                        </div>
                                        <div class="col">
                                            <input type="date" value="<?= $this->input->get('To', TRUE) ?>"name="To" class="form-control">
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-lg btn-info"><span class="fa fa-search"></span></button>
                                            <a href="<?= base_url('laporan/log_lokasi_detail/' . $mobil['id_device']) . '?' . 'From=' . $this->input->get('From', TRUE) . '&' . 'To=' . $this->input->get('To', TRUE) ?>" class="btn btn-lg btn-warning"><span class="fa fa-print"></span></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                          
                            <div class="row">
                                <div class="col">
                                    <a href="<?= base_url('dashboard/log_lokasi_map/' . $mobil['id_device']) ?>" class="btn btn-primary btn-lg text-white">
                                        <span class="fa fa-map"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Log Lokasi Plat <?= $mobil['plat_nomor'] ?></h5>
                        </div>
                        <div id="log_lokasi ">
                            <div class="table-responsive">
                            <table class="table table-hover align-middle" id="table" id="lkTable">
                                <thead>
                                    <tr>
                                        <th>Latitude</th>
                                        <th>Longitude</th>
                                        <th>Status</th>
                                        <th>Status Relay</th>
                                        <th>Waktu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($log_lokasi as $lk) : ?>
                                        <tr>
                                            <td><?= $lk['latitude'] ?></td>
                                            <td><?= $lk['longitude'] ?></td>
                                            <td><?= $lk['status'] ?></td>
                                            <td><?= $lk['status_relay'] ?></td>
                                            <td><?= $lk['waktu'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            </div>
                           
                        </div>

                    </div>
                </div>

            </div>
        </main>
    </div>
</div>


<?php $this->load->view('templates/dashboard_footer') ?>