<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="<?= base_url('dashboard/tambah_mobil') ?>" class="btn btn-primary mb-3 text-white">Tambah</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Mobil</h5>
                        </div>
                        <div class="card-body ">
                            <div class="container ">
                                <div class="table-responsive">
                        <?= $this->session->flashdata('message'); ?>

                                    <table class="table table-hover table-condensed">
                                        <thead>
                                            <tr>
                                                <th>ID Device</th>
                                                <th>Plat Nomor</th>
                                                <th>Status</th>
                                                <th>Mesin</th>
                                                <th>Mode</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($mobil as $m) : ?>
                                            <tr style="height:150px">
                                                    <td><?= $m['id_device'] ?></td>
                                                    <td><?= $m['plat_nomor'] ?></td>
                                                    <td>
                                                        <span <?php if ($m['status_mesin']) : ?> class="badge bg-success" <?php else : ?> class="badge bg-danger" <?php endif ?>>
                                                            <?php if ($m['status_mesin']) : ?> On <?php else : ?> Off <?php endif ?>
                                                        </span>
                                                        <span <?php if ($m['mode'] == 'auto') : ?> class="badge bg-info" <?php else : ?> class="badge bg-warning" <?php endif ?>>
                                                            <?= ($m['mode']) ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <span class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>
                                                            </span>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <form action="<?= base_url('dashboard/status_mesin/' . $m['id_mobil']) ?>" method="post">
                                                                    <input type="hidden" value="1" name="status_mesin">
                                                                    <button type="submit" class="dropdown-item" value="1">On</button>
                                                                </form>
                                                                <form action="<?= base_url('dashboard/status_mesin/' . $m['id_mobil']) ?>" method="post">
                                                                    <input type="hidden" value="0" name="status_mesin">
                                                                    <button type="submit" class="dropdown-item" value="0">Off</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <span class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" >
                                                                <i class="fa fa-cogs"></i>
                                                            </span>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <form action="<?= base_url('dashboard/mode_mesin/' . $m['id_mobil']) ?>" method="post">
                                                                    <input type="hidden" value="auto" name="mode_mesin">
                                                                    <button type="submit" class="dropdown-item" value="auto">Auto</button>
                                                                </form>
                                                                <form action="<?= base_url('dashboard/mode_mesin/' . $m['id_mobil']) ?>" method="post">
                                                                    <input type="hidden" value="manual" name="mode_mesin">
                                                                    <button type="submit" class="dropdown-item" value="manual">Manual</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td class="inline-flex">
                                                        <a href="<?= base_url('dashboard/detail_mobil/' . $m['id_mobil']) ?>" class="btn btn-sm btn-info d-inline"><span class="fa fa-eye"></span></a>
                                                        <a href="<?= base_url('dashboard/ubah_mobil/' . $m['id_mobil']) ?>" class="btn btn-sm btn-success d-inline"><span class="fa fa-edit"></span></a>
                                                        <a href="<?= base_url('dashboard/delete_mobil/' . $m['id_mobil']) ?>" class="btn btn-sm btn-danger d-inline"><span class="fa fa-trash"></span></a>
                                                    </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </main>
    </div>
</div>
<?php $this->load->view('templates/dashboard_footer') ?>