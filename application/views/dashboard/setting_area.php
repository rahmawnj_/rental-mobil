<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="card">
                    <h5 class="card-header"><b>Pengaturan Zona</b></h5>
                    <div class="card-body">
                    <div id="map-canvas" style="height: 400px; width: 100%;"></div>
                <div id="info" style="position: absolute; color: black; height: 200px; "></div>
                    </div>
                </div>

                
            </div>
        </main>
    </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= $gmaps['value'] ?>&libraries=drawing"> </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    var map;
    var mapcanvas;

    let coordinates = [];

    function InitMap() {
        var location = new google.maps.LatLng(-6.335648174, 108.3190108);
        mapOptions = {
            zoom: 8,
            center: location,
            mapTypeId: google.maps.MapTypeId.RoadMap
        }
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions)
        var all_overlays = [];
        var selectedShape;
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON,
                ]
            },

            circleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 0.2,
                strokeWeight: 3,
                clickable: true,
                editable: true
            },
            rectangleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 0.2,
                strokeWeight: 3,
                clickable: true,
                editable: true
            },
            polygonOptions: {
                clickable: true,
                draggable: false,
                editable: true,
                fillColor: '#ADFF2F',
                fillOpacity: 0.5
            },

        })
        drawingManager.setMap(map)

        function clearSelection() {
            if (selectedShape) {
                selectedShape.setEditable(false)
                selectedShape = null
            }
        }

        function setSelection(shape) {
            clearSelection()
            stopDrawing()
            selectedShape = shape
            shape.setEditable(true)
        }

        function stopDrawing() {
            drawingManager.setMap(null)
        }

        function deleteSelectedShape() {
            if (selectedShape) {
                selectedShape.setMap(null)
                drawingManager.setMap(map)
                coordinates.splice(0, coordinates.length)
                document.getElementById('info').innerHTML = ''
            }
        }

        function saveSelectedShape(coordinates) {
            console.log(coordinates)
        }

        function CenterControl(controlDiv, map) {
            var controlUI = document.createElement('div')
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.backgroundColor = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,.3)';
            controlUI.style.cursor = 'pointer',
                controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Select to delete the shape';
            controlDiv.appendChild(controlUI)
            var controlText = document.createElement('div')
            controlText.style.color = 'rgb(25, 25, 25)';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px'
            controlText.innerHTML = 'Hapus Area Terpilih';
            controlUI.appendChild(controlText)
            controlUI.addEventListener('click', function() {
                deleteSelectedShape()
            })
        }

        function SaveControl(controlDiv, event) {
            var controlUI = document.createElement('div')
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.backgroundColor = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,.3)';
            controlUI.style.cursor = 'pointer',
                controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Select to Save the shape';
            controlDiv.appendChild(controlUI)
            var controlText = document.createElement('div')
            controlText.style.color = 'rgb(25, 25, 25)';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px'
            controlText.innerHTML = 'Save Area';
            controlUI.appendChild(controlText)

            coordinates.splice(0, coordinates.length)
            var len = event.getPath().getLength()
            for (var i = 0; i < len; i++) {
                coordinates.push(event.getPath().getAt(i).toUrlValue(5))
            }
            controlUI.addEventListener('click', function() {
                $.post("<?= base_url('api/setting_area') ?>", {
                        coordinates: coordinates
                    },
                    function(data, status) {
                        console.log(data)
                    });
                controlUI.remove();
            })
        }

        function getPolygonCoords(newShape) {
            coordinates.splice(0, coordinates.length)
            var len = newShape.getPath().getLength()
            for (var i = 0; i < len; i++) {
                coordinates.push(newShape.getPath().getAt(i).toUrlValue(5))
            }
            document.getElementById('info').innerHTML = coordinates
            console.log(coordinates)
            return coordinates
        }

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function(event) {
            event.getPath().getLength();
            google.maps.event.addListener(event, 'dragend', getPolygonCoords(event));

            var saveControlDiv = document.createElement('div')
            var saveControl = new SaveControl(saveControlDiv, event)
            saveControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(saveControlDiv)

            google.maps.event.addListener(event.getPath(), 'insert_at', function() {
                coordinates.splice(0, coordinates.length)
                var len = event.getPath().getLength()
                for (let i = 0; i < len; i++) {
                    document.getElementById("info").innerHTML = coordinates
                    coordinates.push(event.getPath().getAt(i).toUrlValues(5))
                }
                document.getElementById('info').innerHTML = coordinates;

                console.log(coordinates)
            })
            google.maps.event.addListener(event.getPath(), 'set_at', function() {
                coordinates.splice(0, coordinates.length)
                var len = event.getPath().getLength()
                for (let i = 0; i < len; i++) {
                    document.getElementById('info').innerHTMLL = coordinates
                    coordinates.push(event.getPath().getAt(i).toUrlValues(5))
                }
                document.getElementById('info').innerHTML = coordinates
            })
        })

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
            all_overlays.push(event)
            if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                drawingManager.setDrawingMode(null)

                var newShape = event.overlay;
                newShape.type = event.type;
                google.maps.event.addListener(newShape, 'click', function() {
                    setSelection(newShape)
                })
                setSelection(newShape)

            }
        })

        var centerControlDiv = document.createElement('div')
        var centerControl = new CenterControl(centerControlDiv, map)

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv)


    }
    InitMap()
</script>
<?php $this->load->view('templates/dashboard_footer') ?>