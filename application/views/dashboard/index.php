<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="card">
                        <div class="card-header">
                            <b>
                                Mobil
                            </b>
                        </div>
                        <div class="card-body">
                            <div id="map" style="height: 400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<script defer src="https://maps.googleapis.com/maps/api/js?key=<?= $gmaps['value'] ?>&callback=initialize" type="text/javascript"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script type="text/javascript">
    function initialize() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,
            center: new google.maps.LatLng(-0.789275, 113.921327),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow();

        $.get("<?= base_url('api/area') ?>", function(data, status) {
            var area = JSON.parse(data)
            const transformArea = (area = []) => {
                const res = [];
                const map = {};
                let i, j, curr;
                for (i = 0, j = area.length; i < j; i++) {
                    curr = area[i];
                    if (!(curr.id_koordinat in map)) {
                        map[curr.id_koordinat] = {
                            id_koordinat: curr.id_koordinat,
                            id_koordinats: []
                        };
                        res.push(map[curr.id_koordinat]);
                    };
                    map[curr.id_koordinat].id_koordinats.push(curr);
                };
                return res;
            };
            var polygons = [];
            for (let index = 0; index < transformArea(area).length; index++) {
                var destinations = new google.maps.MVCArray();
                for (var i = 0; i < transformArea(area)[index].id_koordinats.length; i++) {
                    destinations.push(new google.maps.LatLng(transformArea(area)[index].id_koordinats[i]['latitude'], transformArea(area)[index].id_koordinats[i]['longitude']));
                }
                var polygonOptions = {
                    path: destinations,
                    strokeColor: "#ff0000",
                    fillColor: "#00ff00"
                };
                var polygon = new google.maps.Polygon(polygonOptions)
                polygon.setMap(map)
                polygons.push({
                    id_mobil: area[index].id_mobil,
                    polygon: polygon
                })
            }
        })
        setInterval(() => {
            var marker, i;
            var uniqueId = 0;
            var markers = [];

            $.get("<?= base_url('api/pin') ?>", function(data, status) {
                var pin = JSON.parse(data);

                for (i = 0; i < pin.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(pin[i]['latitude'], pin[i]['longitude']),
                        map: map,
                        icon: {
                            url: "<?= base_url('assets/img/car.png') ?>",
                            scaledSize: new google.maps.Size(50, 25)
                        }
                    })
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(pin[i]['plat_nomor']);
                            infowindow.open(map, marker);
                        }
                    })(marker, i))

                    marker.id = uniqueId;
                    markers.push(marker);
                    if (i == (pin.length - 1)) {
                        setInterval(() => {
                            for (let index = 0; index < pin.length; index++) {
                                markers[index].setMap(null);
                            }
                        }, 2000)
                    }
                    uniqueId++;
                }
            });
        }, 2000)
    }
</script>

<?php $this->load->view('templates/dashboard_footer') ?>