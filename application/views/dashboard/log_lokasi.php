<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="card">
                    <h5 class="card-header"><b>Log Lokasi</b></h5>
                    <div class="card-body">
                        <ol class="list-group list-group-numbered">
                            <?php foreach ($mobil as $m) : ?>
                                <li class="list-group-item">
                                    <?= $m['plat_nomor'] ?>
                                    <a href="<?= base_url('dashboard/log_lokasi_detail/' . $m['id_device']) ?>" class="btn btn-info float-right text-white">Info</a>
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php $this->load->view('templates/dashboard_footer') ?>