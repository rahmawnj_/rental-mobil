<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="card">
                    <div class="card-header">
                        Pengaturan
                    </div>
                    <div class="card-body">
                        <?= $this->session->flashdata('message'); ?>
                        <form action="<?= base_url('dashboard/setting_gmaps') ?>" method="POST">
                            <input type="hidden" name="id_setting" value="<?= $gmaps['id_setting'] ?>" class="form-control" id="id_setting">
                            <div class="mb-3">
                                <label for="gmaps_api_key" class="form-label"><b>GMAPS</b></label>
                                <input type="gmaps_api_key" name="gmaps_api_key" value="<?= $gmaps['value'] ?>" class="form-control" id="gmaps_api_key">
                                <?= form_error('gmaps_api_key') ?>
                            </div>
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary mb-3">Simpan</button>
                            </div>
                        </form>
                        <hr>
                        <form action="<?= base_url('dashboard/setting_telegram') ?>" method="POST">
                            <input type="hidden" name="id_telegram_token" value="<?= $telegram_token['id_setting'] ?>" class="form-control" id="id_telegram_token">
                            <input type="hidden" name="id_telegram_id_chat" value="<?= $telegram_id_chat['id_setting'] ?>" class="form-control" id="id_telegram_id_chat">
                            <div class="mb-3">
                                <label for="telegram_token" class="form-label"><b>Token</b></label>
                                <input type="telegram_token" name="telegram_token" value="<?= $telegram_token['value'] ?>" class="form-control" id="telegram_token">
                                <?= form_error('telegram_token') ?>
                            </div>
                            <div class="mb-3">
                                <label for="telegram_id_chat" class="form-label"><b>Id Chat</b></label>
                                <input type="telegram_id_chat" name="telegram_id_chat" value="<?= $telegram_id_chat['value'] ?>" class="form-control" id="telegram_id_chat">
                                <?= form_error('telegram_id_chat') ?>
                            </div>
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary mb-3">Simpan</button>
                                <a href="<?= base_url('dashboard/tes_telegram') ?>" target="_blank" class="btn btn-warning mb-3">Tes</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php $this->load->view('templates/dashboard_footer') ?>