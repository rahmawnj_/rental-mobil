<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="col-lg-12 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <form action="<?= base_url('dashboard/log_lokasi_map/' . $mobil['id_device']) ?>" method="get">
                                    <div class="input-group">
                                        <div class="col">
                                            <input type="date" value="<?= $this->input->get('From', TRUE) ?>" name="From" id="" class="form-control">
                                        </div>
                                        <div class="col">
                                            <input type="date" value="<?= $this->input->get('From', TRUE) ?>" name="To" class="form-control">
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-lg btn-warning"><span class="fa fa-search"></span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Riwayat Perjalanan Plat <?= $mobil['plat_nomor'] ?></h5>
                        </div>
                        <div id="log_lokasi">
                            <div id="map" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script defer src="https://maps.googleapis.com/maps/api/js?key=<?= $gmaps['value'] ?>&callback=initialize" type="text/javascript"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script type="text/javascript">
    function initialize() {
        $(document).ready(function() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 5,
                center: new google.maps.LatLng(-0.789275, 113.921327),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();

            var i;
            var uniqueId = 0;

            var track = JSON.parse('<?= $log_lokasi ?>')
            const transformTrack = (track = []) => {
                const res = [];
                const map = {};
                let i, j, curr;
                for (i = 0, j = track.length; i < j; i++) {
                    curr = track[i];
                    if (!(curr.id_device in map)) {
                        map[curr.id_device] = {
                            id_device: curr.id_device,
                            id_devices: []
                        };
                        res.push(map[curr.id_device]);
                    };
                    map[curr.id_device].id_devices.push(curr);
                };
                return res;
            };
            var polylines = [];
            for (let index = 0; index < transformTrack(track).length; index++) {
                var destinations = new google.maps.MVCArray();
                for (var i = 0; i < transformTrack(track)[index].id_devices.length; i++) {
                    destinations.push(new google.maps.LatLng(transformTrack(track)[index].id_devices[i]['latitude'], transformTrack(track)[index].id_devices[i]['longitude']));
                }
                var polylineOptions = {
                    path: destinations,
                    strokeColor: "#ff0000",
                    strokeWeight: 4
                };
                var polyline = new google.maps.Polyline(polylineOptions)
                polyline.setMap(map)
            }

        })
    }
</script>

<?php $this->load->view('templates/dashboard_footer') ?>