<?php $this->load->view('templates/dashboard_header') ?>
<div class="wrapper">
    <?php $this->load->view('templates/dashboard_sidebar') ?>
    <div class="main" id="panel">
        <?php $this->load->view('templates/dashboard_navbar') ?>
        <main class="content">
            <div class="container-fluid p-0">
                <div class="card">
                    <div class="card-header">
                        Akun
                    </div>
                    <div class="card-body">
                        <?= $this->session->flashdata('message'); ?>
                        <form action="<?= base_url('auth/change_profile') ?>" method="POST">
                            <input type="hidden" name="id_user" value="<?= $user['id_user'] ?>" class="form-control" id="id_user">
                            <div class="mb-3">
                                <label for="name" class="form-label"><b>Nama</b></label>
                                <input type="text" name="name" value="<?= $user['name'] ?>" class="form-control" id="name">
                                <?= form_error('name') ?>
                            </div>
                            <div class="mb-3">
                                <label for="username" class="form-label"><b>Username</b></label>
                                <input type="text" autocomplete="off" name="username" value="<?= $user['username'] ?>" class="form-control" id="username">
                                <?= form_error('username') ?>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label"><b>Password</b></label>
                                <input type="password" autocomplete="off" name="password" value="" class="form-control" id="password">
                                <?= form_error('password') ?>
                            </div>
                            <div class="mb-3">
                                <label for="password_verify" class="form-label"><b>Verifikasi Password</b></label>
                                <input type="password" autocomplete="off" name="password_verify" value="" class="form-control" id="password_verify">
                                <?= form_error('password_verify') ?>
                            </div>
                           
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary mb-3">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php $this->load->view('templates/dashboard_footer') ?>